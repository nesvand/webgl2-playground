/**
 *
 * @param {number} range
 * @returns {number}
 */
export function randomInt(range) {
  return Math.floor(Math.random() * range);
}
