/**
 *
 * @param {WebGL2RenderingContext} gl
 * @param {number} type
 * @param {string} source
 * @returns {WebGLShader | null}
 */
export function createShader(gl, type, source) {
  // Create the shader object
  const shader = gl.createShader(type);

  // Set the shader source
  gl.shaderSource(shader, source);

  // Compiler the shader
  gl.compileShader(shader);

  // Check if successfully compiled
  const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);

  if (!success) {
    throw Error(`Could not compile shader: ${gl.getShaderInfoLog(shader)}`);
  }

  return shader;
}
