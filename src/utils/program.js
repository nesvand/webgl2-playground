import { createShader } from './';

/**
 *
 * @param {WebGL2RenderingContext} gl
 * @param {WebGLShader} vertexShader
 * @param {WebGLShader} fragmentShader
 * @returns {WebGLProgram | null}
 */
export function createProgram(gl, vertexShader, fragmentShader) {
  // Create a program
  const program = gl.createProgram();

  // Attach the shaders
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);

  // Link the program
  gl.linkProgram(program);

  // Check for a successful program link
  const success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!success) {
    throw Error(`Program failed to link: ${gl.getProgramInfoLog(program)}`);
  }

  return program;
}

/**
 *
 * @param {WebGL2RenderingContext} gl
 * @param {string} vertexShaderSource
 * @param {string} fragmentShaderSource
 * @returns {WebGLProgram}
 */
export function createProgramFromSource(
  gl,
  vertexShaderSource,
  fragmentShaderSource
) {
  const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  const fragmentShader = createShader(
    gl,
    gl.FRAGMENT_SHADER,
    fragmentShaderSource
  );

  return createProgram(gl, vertexShader, fragmentShader);
}
