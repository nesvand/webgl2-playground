/**
 *
 * @param {WebGL2RenderingContext} gl
 * @param {WebGLFramebuffer} fbo
 * @param {number} width
 * @param {number} height
 * @param {WebGLUniformLocation} resolutionUniformLocation
 */
export function setFramebuffer(
  gl,
  fbo,
  width,
  height,
  resolutionUniformLocation
) {
  // make this the framebuffer we are rendering to.
  gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

  // Tell the shader the resolution of the framebuffer.
  gl.uniform2f(resolutionUniformLocation, width, height);

  // Tell WebGL how to convert from clip space to pixels
  gl.viewport(0, 0, width, height);
}
