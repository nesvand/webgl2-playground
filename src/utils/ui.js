/**
 * @type {{name: string, on?: boolean}[]}
 */
export const effects = [
  { name: 'gaussianBlur3', on: true },
  { name: 'gaussianBlur3', on: true },
  { name: 'gaussianBlur3', on: true },
  { name: 'sharpness' },
  { name: 'sharpness' },
  { name: 'sharpness' },
  { name: 'sharpen' },
  { name: 'sharpen' },
  { name: 'sharpen' },
  { name: 'unsharpen' },
  { name: 'unsharpen' },
  { name: 'unsharpen' },
  { name: 'emboss', on: true },
  { name: 'edgeDetect' },
  { name: 'edgeDetect' },
  { name: 'edgeDetect3' },
  { name: 'edgeDetect3' },
];
