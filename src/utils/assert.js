/**
 *
 * @param {any[]} objects
 * @param {string} errorMessage
 */
export function assert(objects, errorMessage) {
  for (const object of objects) {
    if (!object) {
      throw Error(errorMessage);
    }
  }
}
