import { createProgramFromSource, resizeCanvasToDisplaySize } from './utils';
import { mat3 } from 'gl-matrix';

import './index.css';
import vertexShaderSource from './shaders/vert.glsl';
import fragmentShaderSource from './shaders/frag.glsl';

/**
 * @param {WebGL2RenderingContext} gl
 */
function draw(gl) {
  /*
  // =======
  // Program
  // =======
  */

  const program = createProgramFromSource(
    gl,
    vertexShaderSource,
    fragmentShaderSource
  );

  /*
  // =============
  // Locations
  // =============
  */

  // Attribs
  const positionAttributeLocation = gl.getAttribLocation(program, 'a_position');

  // Uniforms
  const colorLocation = gl.getUniformLocation(program, 'u_color');
  const matrixLocation = gl.getUniformLocation(program, 'u_matrix');

  /*
  // =========================
  // VAO - Vertex Array Object
  // =========================
  */
  const vao = gl.createVertexArray();
  gl.bindVertexArray(vao);

  /*
  // ===============
  // Position Buffer
  // ===============
  */
  const positionBuffer = gl.createBuffer();
  gl.enableVertexAttribArray(positionAttributeLocation);

  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  setGeometryF(gl);

  gl.vertexAttribPointer(positionAttributeLocation, 2, gl.FLOAT, false, 0, 0);

  /*
  // ============
  // Render Scene
  // ============
  */

  resizeCanvasToDisplaySize(gl.canvas);

  // Tell WebGL how to convert from clip space to pixels
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  // Clear the canvas
  gl.clearColor(0, 0, 0, 0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // Tell it to use our program (pair of shaders)
  gl.useProgram(program);

  // Bind the attribute/buffer set we want.
  gl.bindVertexArray(vao);

  // Set the color
  const color = [Math.random(), Math.random(), Math.random(), 1];
  gl.uniform4fv(colorLocation, color);

  // Compute the matrix
  const matrix = mat3.create();
  mat3.projection(matrix, gl.canvas.clientWidth, gl.canvas.clientHeight);
  mat3.translate(matrix, matrix, [450, 100]);
  mat3.rotate(matrix, matrix, Math.PI * 0.25);
  mat3.scale(matrix, matrix, [3, 3]);

  gl.uniformMatrix3fv(matrixLocation, false, matrix);

  gl.drawArrays(gl.TRIANGLES, 0, 18);
}

function main() {
  const webGL2CanvasElement = document.getElementById('c');
  if (!(webGL2CanvasElement instanceof HTMLCanvasElement)) {
    throw Error('no canvas element found');
  }

  const webGL2Context = webGL2CanvasElement.getContext('webgl2');
  draw(webGL2Context);
}

main();

/**
 * Fill the current ARRAY_BUFFER buffer
 * with the values that define a letter 'F'.
 *
 * @param {WebGL2RenderingContext} gl
 */
function setGeometryF(gl) {
  gl.bufferData(
    gl.ARRAY_BUFFER,
    new Float32Array([
      // left column
      0,
      0,
      30,
      0,
      0,
      150,
      0,
      150,
      30,
      0,
      30,
      150,

      // top rung
      30,
      0,
      100,
      0,
      30,
      30,
      30,
      30,
      100,
      0,
      100,
      30,

      // middle rung
      30,
      60,
      67,
      60,
      30,
      90,
      30,
      90,
      67,
      60,
      67,
      90,
    ]),
    gl.STATIC_DRAW
  );
}
