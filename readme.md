# WebGL2 Playground

[![Maintainability](https://api.codeclimate.com/v1/badges/db201073f2a709168aeb/maintainability)](https://codeclimate.com/github/nesvand/webgl2-playground/maintainability)
[![Known Vulnerabilities](https://snyk.io/test/github/nesvand/webgl2-playground/badge.svg)](https://snyk.io/test/github/nesvand/webgl2-playground)

A personal playground to experiment and learn WebGL2 and any other tools (Typescript, ParcelJS, etc.) I happen to come across

Currently following topics outlined at [https://webgl2fundamentals.org/](https://webgl2fundamentals.org/)
